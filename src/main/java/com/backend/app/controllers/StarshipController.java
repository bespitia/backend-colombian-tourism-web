package com.backend.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;


import com.backend.app.models.ResponseApiTrait;
import com.backend.app.models.StarshipsModel;
import com.backend.app.persistence.StarshipsEloquent;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/starship")
public class StarshipController {
    @Autowired
    StarshipsEloquent starshipsEloquent;

    @Operation(summary = "Get an starship List")
    @GetMapping()
    public ResponseApiTrait list(){
        return new ResponseApiTrait(this.starshipsEloquent.getStarships(),"",0);
    }

    @Operation(summary = "create or update starship data")
    @PostMapping()
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseApiTrait store(@RequestBody @Valid StarshipsModel starshipsModel){
        StarshipsModel obj = this.starshipsEloquent.storeUpdateStarship(starshipsModel);
        return new ResponseApiTrait(obj, "",0);
    }

    @Operation(summary = "query the starship by id")
    @GetMapping(path = "/{id}")
    public ResponseApiTrait getUserById(@PathVariable String id){
        return new ResponseApiTrait( this.starshipsEloquent.getById(id), "",0);
    }

    @Operation(summary = "delete the starship by id")
    @DeleteMapping()
    public ResponseApiTrait deleteById(@RequestParam String id){
        boolean delete = this.starshipsEloquent.deleteStarship(id);
        if(delete){
            return new ResponseApiTrait( "starship deleted successfully", "",0);
        }else{
            return new ResponseApiTrait( "starship not found", "Bad Request",404);
        }
    }

    @Operation(summary = "gets ship by number of passengers")
    @GetMapping(path = "/passengers/{passengers}")
    public ResponseApiTrait findByNumberPassengers(@PathVariable Long passengers){
        StarshipsModel starship = this.starshipsEloquent.getStarshipByAbility(passengers);
        if(starship != null){
            return new ResponseApiTrait( starship, "",0);
        }else{
            return new ResponseApiTrait( "starship not found", "Bad Request",404);
        }
    }

}
