package com.backend.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.backend.app.models.ResponseApiTrait;
import com.backend.app.models.UserModel;
import com.backend.app.persistence.UserEloquent;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserEloquent userEloquent;

    @Operation(summary = "Get an User List")
    @GetMapping()
    public ResponseApiTrait list() {
        return new ResponseApiTrait(this.userEloquent.getUsers(), "", 0);
    }

    @Operation(summary = "create or update user data")
    @PostMapping()
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseApiTrait store(@RequestBody @Valid UserModel user) {
        UserModel obj = this.userEloquent.storeUpdateUser(user);
        return new ResponseApiTrait(obj, "", 0);
    }

    @Operation(summary = "brings 10 users sorted by name")
    @GetMapping(path = "/fetch-order")
    public ResponseApiTrait fetchOrderUsers() {
        return new ResponseApiTrait(this.userEloquent.fetchOrderUsers(), "", 0);
    }

    @Operation(summary = "shows the most used word in names and surnames")
    @GetMapping(path = "/fetch-count")
    public ResponseApiTrait fetchCountUsers() {
        return new ResponseApiTrait(this.userEloquent.fetchCountUsers(), "", 0);
    }

    @Operation(summary = "query the user by id")
    @GetMapping(path = "/{id}")
    public ResponseApiTrait getUserById(@PathVariable String id) {
        return new ResponseApiTrait(this.userEloquent.getById(id), "", 0);
    }

    @Operation(summary = "query the user by age")
    @GetMapping(path = "/age")
    public ResponseApiTrait getUserByAge(@RequestParam int age) {
        Object response = this.userEloquent.getByAge(age);
        if (response != null) {
            return new ResponseApiTrait(response, "", 0);
        } else {
            return new ResponseApiTrait("no users older than the submitted age were found", "Bad Request", 404);
        }
    }

    @Operation(summary = "delete the user by id")
    @DeleteMapping()
    public ResponseApiTrait deleteById(@RequestParam String id) {
        boolean delete = this.userEloquent.deleteUser(id);
        if (delete) {
            return new ResponseApiTrait("user deleted successfully", "", 0);
        } else {
            return new ResponseApiTrait("user not found", "Bad Request", 404);
        }
    }

}
