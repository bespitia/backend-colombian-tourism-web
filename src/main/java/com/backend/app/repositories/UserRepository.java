package com.backend.app.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.backend.app.models.UserModel;

@Repository
public interface UserRepository extends CrudRepository<UserModel, String> {
	    
    public abstract UserModel findFirstByAgeGreaterThanOrderByAgeAsc(Integer age);
}