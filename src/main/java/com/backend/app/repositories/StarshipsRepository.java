package com.backend.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.backend.app.models.StarshipsModel;

@Repository
public interface StarshipsRepository extends JpaRepository<StarshipsModel, String> {

    @Query("SELECT s FROM StarshipsModel s WHERE s.numberPassengers >= :passengers AND (s.consumables LIKE '%year%' OR s.consumables LIKE '%month%' OR s.consumables LIKE '%week%') AND (array_to_string(s.films,',') like '%/5/%' or array_to_string(s.films,',') like '%/4/%' or array_to_string(s.films,',') like '%/6/%') ORDER BY s.maxAtmospheringSpeed ASC limit 1")
    public abstract StarshipsModel findByNumberPassengers(@Param("passengers") Long passengers);

}