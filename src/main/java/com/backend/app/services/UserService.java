package com.backend.app.services;

import org.springframework.web.client.RestTemplate;
import com.backend.app.models.UserModel;
import com.backend.app.resources.UserResource;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;


public class UserService {

    protected RestTemplate restTemplate;

    public UserService() {
        this.restTemplate = new RestTemplate();;

    }

    public ArrayList<UserModel> getUserService(int quantity){
        UserResource userResource = new UserResource();
        try {
            disableSSLVerification();   
        } catch (Exception e) {
        }

        String  response = restTemplate.getForObject("https://randomuser.me/api/?results="+quantity, String.class);
        
        ArrayList<UserModel> listUsers = new ArrayList<UserModel>();
        JSONObject jsonObj = new JSONObject(response.toString());

        for (Object person : jsonObj.getJSONArray("results")) {
            listUsers.add(userResource.parseUserData(person));
        }
        
        return listUsers;
    }
    
    public static void disableSSLVerification() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[] { new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        } }, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
    }
}
