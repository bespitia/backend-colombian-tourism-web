package com.backend.app.resources;

import java.util.List;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.backend.app.models.StarshipsModel;

/**
 * Class StarshipsResource transform user data
 */
public class StarshipsResource {
    /**
     * Resolve the services resource to an StarshipsModel.
     *
     * @param Object $request
     *
     * @return StarshipsModel Data transformed
     */
    public StarshipsModel parseStarshipsData(Object request) {
        JSONObject starship = new JSONObject(request.toString());
        JSONArray films = starship.getJSONArray("films");
        List<String> stringList = new ArrayList<String>();
        for (int i = 0; i < films.length(); i++) {
            stringList.add(films.getString(i));
        }
        return new StarshipsModel(starship.getString("name"), Long.parseLong(starship.getString("length").replaceAll("[^0-9]", "")),starship.getString("max_atmosphering_speed").replaceAll("[^0-9]", ""),starship.getString("consumables"),stringList);
    }
}
