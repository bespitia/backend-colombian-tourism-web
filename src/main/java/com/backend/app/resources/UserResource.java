package com.backend.app.resources;

import org.json.JSONObject;

import com.backend.app.models.UserModel;

/**
 * Class UserResource transform user data
 */
public class UserResource {
    /**
     * Resolve the services resource to an UserModel.
     *
     * @param Object $request
     *
     * @return UserModel Data transformed
     */
    public UserModel parseUserData(Object request) {
        JSONObject user = new JSONObject(request.toString());

        JSONObject nombre = user.getJSONObject("name");
        JSONObject birthdate = user.getJSONObject("dob");

        return new UserModel(nombre.getString("first"), nombre.getString("last"), user.getString("gender"),
                user.getString("email"), birthdate.getInt("age"));
    }
}
