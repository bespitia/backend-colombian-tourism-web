package com.backend.app.seeders;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.backend.app.models.UserModel;
import com.backend.app.repositories.UserRepository;
import com.backend.app.services.UserService;

@Component
public class DataLoader implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... args) throws Exception {
		UserService service = new UserService();

		ArrayList<UserModel> users = (ArrayList<UserModel>) userRepository.findAll();
		if (users.size() == 0) {
			ArrayList<UserModel> newUsers = service.getUserService(20);
			userRepository.saveAll(newUsers);
		}
	}
}