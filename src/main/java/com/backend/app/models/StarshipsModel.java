package com.backend.app.models;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name = "starships")
public class StarshipsModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(unique = true, nullable = false)
    private String id;

    protected String name;
    protected Long numberPassengers;
    protected String maxAtmospheringSpeed;
    protected String consumables;
    protected List<String> films;

    public StarshipsModel() {
	}
	
    public StarshipsModel(String name, Long numberPassengers, String maxAtmospheringSpeed, String consumables,
            List<String> films) {
        this.name = name;
        this.numberPassengers = numberPassengers;
        this.maxAtmospheringSpeed = maxAtmospheringSpeed;
        this.consumables = consumables;
        this.films = films;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumberPassengers() {
        return numberPassengers;
    }

    public void setNumberPassengers(Long numberPassengers) {
        this.numberPassengers = numberPassengers;
    }

    public String getMaxAtmospheringSpeed() {
        return maxAtmospheringSpeed;
    }

    public void setMaxAtmospheringSpeed(String maxAtmospheringSpeed) {
        this.maxAtmospheringSpeed = maxAtmospheringSpeed;
    }

    public String getConsumables() {
        return consumables;
    }

    public void setConsumables(String consumables) {
        this.consumables = consumables;
    }

    public List<String> getFilms() {
        return films;
    }

    public void setFilms(List<String> films) {
        this.films = films;
    }

    

}
