package com.backend.app.models;

public class ResponseApiTrait {
    private Object data;
    private String message;
    private int statusCode;
    private String service;

    public ResponseApiTrait(Object data, String message,int statusCode) {
            this.data = data;
            this.message = message != "" ? message: "Success operation";
            this.statusCode= statusCode != 0 ? statusCode: 200;
            this.service= "SPRING-BOOT";
        }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    
    
}
