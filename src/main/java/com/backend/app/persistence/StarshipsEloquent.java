package com.backend.app.persistence;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.app.models.StarshipsModel;
import com.backend.app.repositories.StarshipsRepository;

@Service
public class StarshipsEloquent {
    @Autowired
    StarshipsRepository starshipsRepository;

    public ArrayList<StarshipsModel> getStarships() {
        return (ArrayList<StarshipsModel>) starshipsRepository.findAll();
    }

    public StarshipsModel storeUpdateStarship(StarshipsModel Starship) {
        return starshipsRepository.save(Starship);
    }

    public Optional<StarshipsModel> getById(String Id) {
        return starshipsRepository.findById(Id);
    }

    public boolean deleteStarship(String Id) {
        try {
            starshipsRepository.deleteById(Id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public StarshipsModel getStarshipByAbility(long passengers) {
        return starshipsRepository.findByNumberPassengers(passengers);
    }
}
