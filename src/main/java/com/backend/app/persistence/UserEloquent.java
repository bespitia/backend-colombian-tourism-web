package com.backend.app.persistence;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Optional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.backend.app.models.UserModel;
import com.backend.app.repositories.UserRepository;
import com.backend.app.services.UserService;

@Service
public class UserEloquent {
    @Autowired
    UserRepository userRepository;
    UserService userService;

    public UserEloquent() {
        this.userService = new UserService();
    }

    public ArrayList<UserModel> getUsers() {
        return (ArrayList<UserModel>) userRepository.findAll();
    }

    public UserModel storeUpdateUser(UserModel user) {
        return userRepository.save(user);
    }

    public Optional<UserModel> getById(String Id) {
        return userRepository.findById(Id);
    }

    public UserModel getByAge(int Age) {
        return userRepository.findFirstByAgeGreaterThanOrderByAgeAsc(Age);
    }

    public boolean deleteUser(String Id) {
        try {
            userRepository.deleteById(Id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<UserModel> fetchOrderUsers() {
        Iterable<UserModel> users = userRepository.saveAll(userService.getUserService(10));
        List<UserModel> userList = StreamSupport.stream(users.spliterator(), false)
                .collect(Collectors.toList());
        userList.sort(Comparator.comparing(UserModel::getName));
        return userList;
    }

    public Map<String, Object> fetchCountUsers() {
        Iterable<UserModel> users = userRepository.saveAll(userService.getUserService(5));
        List<String> names = new ArrayList<>();
        String text = "";
        for (UserModel userModel : users) {
            names.add(userModel.getName() + " " + userModel.getLastname());
            text += userModel.getName() + userModel.getLastname();
        }

        Map<Character, Integer> frequency = new HashMap<Character, Integer>();
        for (int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);
            if (Character.isLetter(letter)) {
                letter = Character.toLowerCase(letter);
                if (frequency.containsKey(letter)) {
                    int count = frequency.get(letter);
                    frequency.put(letter, count + 1);
                } else {
                    frequency.put(letter, 1);
                }
            }
        }

        String mostFrequentLetter = "";
        int maxFrecuency = 0;

        for (Map.Entry<Character, Integer> entry : frequency.entrySet()) {
            char letter = entry.getKey();
            int frecuency = entry.getValue();
            if (frecuency > maxFrecuency) {
                mostFrequentLetter = Character.toString(letter);
                maxFrecuency = frecuency;
            }
        }

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("list_names", names);
        response.put("most_frequent_letter", mostFrequentLetter);
        return response;
    }
}
