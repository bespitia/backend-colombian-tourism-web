package com.backend.ApiRest;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.backend.app.app;
import com.backend.app.models.ResponseApiTrait;
import com.backend.app.models.StarshipsModel;
import com.backend.app.repositories.StarshipsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = app.class)
public class StarShipRepositoryTest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private StarshipsRepository starshipsRepository;

    private ObjectMapper objectMapper;

    public StarShipRepositoryTest() {
        this.objectMapper = new ObjectMapper();
    }

    @Test
    public void testGetAllStarShips() {
        ResponseEntity<ResponseApiTrait> response = restTemplate.exchange("/starship", HttpMethod.GET, null,
                new ParameterizedTypeReference<ResponseApiTrait>() {
                });
        ResponseApiTrait starShips = response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(starShips);
    }

    @Test
    public void testGetStarshipById() {

        StarshipsModel starship = new StarshipsModel();
        starship.setName("mi nave");
        starship.setMaxAtmospheringSpeed("10000");
        starship.setNumberPassengers((long) 2350);
        starship.setConsumables("1 year");
        StarshipsModel savedStarShip = starshipsRepository.save(starship);

        ResponseEntity<ResponseApiTrait> response = restTemplate.getForEntity("/starship/" + savedStarShip.getId(),
                ResponseApiTrait.class);

        StarshipsModel starshipFind = objectMapper.convertValue(response.getBody().getData(), StarshipsModel.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(starshipFind);
        assertEquals("mi nave", starshipFind.getName());
        assertEquals(2350, starshipFind.getNumberPassengers());
        assertEquals("1 year", starshipFind.getConsumables());
    }

    @Test
    public void testCreateStarship() {
        StarshipsModel starship = new StarshipsModel();
        starship.setName("mi nave");
        starship.setMaxAtmospheringSpeed("10000");
        starship.setNumberPassengers((long) 2350);
        starship.setConsumables("1 year");

        ResponseEntity<ResponseApiTrait> response = restTemplate.postForEntity("/starship", starship,
                ResponseApiTrait.class);
        StarshipsModel createdStarship = objectMapper.convertValue(response.getBody().getData(), StarshipsModel.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(createdStarship);
        assertEquals("mi nave", createdStarship.getName());
        assertEquals(2350, createdStarship.getNumberPassengers());
    }

    @Test
    public void testUpdateStarship() {
        StarshipsModel starship = new StarshipsModel();
        starship.setName("mi nave");
        starship.setMaxAtmospheringSpeed("10000");
        starship.setNumberPassengers((long) 2350);
        starship.setConsumables("1 year");
        StarshipsModel savedStarShip = starshipsRepository.save(starship);

        savedStarShip.setNumberPassengers((long) 50);
        savedStarShip.setName("mi nave nueva");

        ResponseEntity<ResponseApiTrait> response = restTemplate.postForEntity("/starship", savedStarShip,
                ResponseApiTrait.class);
        StarshipsModel updatedStartShipResponse = objectMapper.convertValue(response.getBody().getData(), StarshipsModel.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(updatedStartShipResponse);
        assertEquals(50, updatedStartShipResponse.getNumberPassengers());
        assertEquals("mi nave nueva", updatedStartShipResponse.getName());
    }

    @Test
    public void testDeleteStarship() {
        StarshipsModel starship = new StarshipsModel();
        starship.setName("mi nave");
        starship.setMaxAtmospheringSpeed("10000");
        starship.setNumberPassengers((long) 2350);
        starship.setConsumables("1 year");
        StarshipsModel savedStarShip = starshipsRepository.save(starship);

        String id = savedStarShip.getId();
        restTemplate.delete("/starship?id=" + id);
        ResponseEntity<ResponseApiTrait> response = restTemplate.getForEntity("/starship/" + id,
                ResponseApiTrait.class);
        assertNull(response.getBody().getData());
    }

    @Test
    public void testGetStarshipByPassengers() {

        StarshipsModel starship = new StarshipsModel();
        List<String> films = new ArrayList<String>();
        films.add("https://swapi.dev/api/films/6/");
        starship.setName("mi nave");
        starship.setMaxAtmospheringSpeed("10000000");
        starship.setNumberPassengers((long) 25);
        starship.setConsumables("1 year");
        starship.setFilms(films);
        StarshipsModel savedStarShip = starshipsRepository.save(starship);

        ResponseEntity<ResponseApiTrait> response = restTemplate.getForEntity("/starship/passengers/" + savedStarShip.getNumberPassengers(),
                ResponseApiTrait.class);

        StarshipsModel starshipFind = objectMapper.convertValue(response.getBody().getData(), StarshipsModel.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(starshipFind);
        assertEquals("mi nave", starshipFind.getName());
        assertEquals(25, starshipFind.getNumberPassengers());
        assertEquals("1 year", starshipFind.getConsumables());
    }

}