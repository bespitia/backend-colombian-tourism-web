package com.backend.ApiRest;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.backend.app.app;
import com.backend.app.models.ResponseApiTrait;
import com.backend.app.models.UserModel;
import com.backend.app.repositories.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = app.class)
public class UserRepositoryTest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private UserRepository userRepository;

    private ObjectMapper objectMapper;

    

    public UserRepositoryTest() {
        this.objectMapper = new ObjectMapper();
    }

    @Test
    public void testGetAllUsers() {
        ResponseEntity<ResponseApiTrait> response = restTemplate.exchange("/user", HttpMethod.GET, null, new ParameterizedTypeReference<ResponseApiTrait>() {});
        ResponseApiTrait users = response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(users);
    }

    @Test
    public void testGetUserById() {

        UserModel user = new UserModel();
        user.setName("carlos");
        user.setEmail("carlos@example.com");
        user.setLastname("fuentes");
        user.setAge((byte) 23);
        user.setGender("Male");
        UserModel savedUser = userRepository.save(user);

        ResponseEntity<ResponseApiTrait> response = restTemplate.getForEntity("/user/"+savedUser.getId(), ResponseApiTrait.class);

        UserModel userFind =objectMapper.convertValue(response.getBody().getData(), UserModel.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(userFind);
        assertEquals("carlos", userFind.getName());
        assertEquals(23, userFind.getAge());
        assertEquals("Male", userFind.getGender());
    }

    @Test
    public void testCreateUser() {
        UserModel newUser = new UserModel();
        newUser.setName("carlos");
        newUser.setEmail("carlos@example.com");
        newUser.setLastname("fuentes");
        newUser.setAge((byte) 23);
        newUser.setGender("Male");

        ResponseEntity<ResponseApiTrait> response = restTemplate.postForEntity("/user", newUser, ResponseApiTrait.class);
        UserModel createdUser =objectMapper.convertValue(response.getBody().getData(), UserModel.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(createdUser);
        assertEquals("carlos", createdUser.getName());
        assertEquals(23, createdUser.getAge());
    }

    @Test
    public void testUpdateUser() {
        UserModel user = new UserModel();
        user.setName("carlos");
        user.setEmail("carlos@example.com");
        user.setLastname("fuentes");
        user.setAge((byte) 23);
        user.setGender("Male");
        UserModel savedUser = userRepository.save(user);

        savedUser.setAge((byte)50);
        savedUser.setName("Andrea");

        ResponseEntity<ResponseApiTrait> response = restTemplate.postForEntity("/user", savedUser, ResponseApiTrait.class);
        UserModel updatedUserResponse =objectMapper.convertValue(response.getBody().getData(), UserModel.class);
        
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(updatedUserResponse);
        assertEquals("Andrea", updatedUserResponse.getName());
        assertEquals(50, updatedUserResponse.getAge());
    }

    @Test
    public void testDeleteUser() {
        UserModel user = new UserModel();
        user.setName("carlos");
        user.setEmail("carlos@example.com");
        user.setLastname("fuentes");
        user.setAge((byte) 23);
        user.setGender("Male");
        UserModel savedUser = userRepository.save(user);
        String id = savedUser.getId();
        restTemplate.delete("/user?id="+id);

        ResponseEntity<ResponseApiTrait> response = restTemplate.getForEntity("/user/"+id, ResponseApiTrait.class);
        assertNull(response.getBody().getData());
    }

    @Test
    public void testGetFetchOrder() {
        UserModel user = new UserModel();
        user.setName("carlos");
        user.setEmail("carlos@example.com");
        user.setLastname("fuentes");
        user.setAge((byte) 23);
        user.setGender("Male");
        UserModel savedUser = userRepository.save(user);
        ResponseEntity<ResponseApiTrait> response = restTemplate.getForEntity("/user/age?age="+savedUser.getAge(), ResponseApiTrait.class);
        ResponseApiTrait users = response.getBody();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(users);
    }
}