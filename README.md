# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.0.6/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.0.6/maven-plugin/reference/html/#build-image)

### REQUIREMENTS
This project was developed using Java version 20 and it's necessary to have the JDK downloaded, which can be found at the following link https://www.oracle.com/java/technologies/javase/jdk20-archive-downloads.html

As a reminder, it's necessary to create the database with the name "springboot", in case you want to change it, do it from ApiRest\src\main\resources\application.properties in the spring.datasource.url property.

### EXECUTION
Once the JDK is installed and the database is created, it's necessary to navigate to the project root folder and execute the following command:

    > mvnw.cmd spring-boot:run

Once the project is started, it's possible to start the testing using the following route:
"http://localhost:8080/"

This project includes Swagger documentation dependency, which can be accessed through the following link: "http://localhost:8080/swagger-ui/index.html"

Finally, Junit tests were used, to execute them, it's necessary to execute the command:

    > mvn test